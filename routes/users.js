var express = require('express');
var router = express.Router();


module.exports = function (dbConnection) {
  const { User } = dbConnection;
  /* Create new User. */
  router.post('/', async function (req, res, next) {
    const newUser = await User.create(req);
    res.send(newUser);
  });

  /* Get All Users. */
  router.get('/', async function (req, res, next) {
    const allUsers = await User.findAll();
    res.send(allUsers);
  });

  return router
};
