var express = require('express');
var http = require('http');
const dbConnection = require('./config/dbConnection')
// Import Routes
var usersRouter = require('./routes/users');

var app = express();

// Define middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

var port = process.env.PORT || '3000';
app.set('port', port);

// Define Routes
app.use('/users', usersRouter((dbConnection)));

// Define Server
var server = http.createServer(app);
server.listen(port);
server.on('error', () => { console.error('Failed to listening on :' + port); });
server.on('listening', () => { console.debug('App listening on :' + port); });

module.exports = app;
